import base64
import datetime
import json

import requests
import xmltodict as xmltodict
from flask import Response, request
from requests.auth import HTTPBasicAuth

import os

WSDL_URL = os.environ.get("WSDL_URL")
CERTIFICATE = os.environ.get("CERTIFICATE")
BASIC_AUTH = HTTPBasicAuth(os.environ.get("USERNAME"), os.environ.get("PASSWORD"))


def get_wsdl():
    response = requests.get(
        url="{}?wsdl".format(WSDL_URL),
        auth=BASIC_AUTH,
        cert=CERTIFICATE,
        verify=False
    )
    return Response(response.content, mimetype='text/xml')


def parse_response(response):
    print(response.text)
    json_response = xmltodict.parse(response.text, attr_prefix='')
    try:
        if response.status_code == 200:

            status = json_response["SOAP_ENV:Envelope"]["SOAP_ENV:Body"]["ConsultazioneIndicatoreResponse"][
                "ConsultazioneIndicatoreResult"]["Esito"]
            if status == "OK":
                base64_data = json_response["SOAP_ENV:Envelope"]["SOAP_ENV:Body"]["ConsultazioneIndicatoreResponse"][
                    "ConsultazioneIndicatoreResult"]["XmlEsitoIndicatore"]
                json_response["SOAP_ENV:Envelope"]["SOAP_ENV:Body"]["ConsultazioneIndicatoreResponse"][
                    "ConsultazioneIndicatoreResult"]["XmlEsitoIndicatore"] = xmltodict.parse(
                    base64.decodebytes(base64_data.encode('utf-8')), attr_prefix='')
                return json.dumps(json_response)
            else:
                return json.dumps(json_response)

        else:
            return json.dumps(json_response['soapenv:Envelope']['soapenv:Body']['soapenv:Fault']['faultstring'])
    except Exception as e:
        return json.dumps(str(e))


def get_isee(fiscal_code):
    year = request.args.get('year') or datetime.datetime.now().year

    headers = {"content-type": "application/soap+xml", "SOAPAction": "ConsultazioneIndicatore"}
    response = requests.post(
        url=WSDL_URL,
        data="""<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
  <soapenv:Header/>
  <soapenv:Body>
     <con:ConsultazioneIndicatore xmlns:con="http://soa.inps.it/wsiseepdd/consultazioneindicatorerichiesta.xsd">
         <con:SicurezzaIdentificazioneMittente>
           <con:CodiceEnte>{}</con:CodiceEnte>
           <con:CodiceUfficio>{}</con:CodiceUfficio>
           <con:CFOperatore>{}</con:CFOperatore>
        </con:SicurezzaIdentificazioneMittente>
        <con:request>
           <con:TipoIndicatore>Ordinario</con:TipoIndicatore>
           <con:RicercaCF>
              <con:CodiceFiscale>{}</con:CodiceFiscale>
              <con:DataValidita>{}-12-31T00:00:00.00</con:DataValidita>
              <con:PrestazioneDaErogare>A1.01</con:PrestazioneDaErogare>
          <con:ProtocolloDomandaEnteErogatore>PROTOCOLLO</con:ProtocolloDomandaEnteErogatore>
              <con:StatodomandaPrestazione>Da Erogare</con:StatodomandaPrestazione>
           </con:RicercaCF>
        </con:request>
     </con:ConsultazioneIndicatore>
  </soapenv:Body>
</soapenv:Envelope>
""".format(
            os.environ.get("CODICE_ENTE"),
            os.environ.get("CODICE_UFFICIO"),
            os.environ.get("CFOPERATORE"),
            fiscal_code.upper(),
            year
        ),
        headers=headers,
        auth=BASIC_AUTH,
        cert=CERTIFICATE,
        verify=False
    )

    if response.status_code == 500:
        response = Response("An error occurred while fetching ISEE", mimetype="application/json")
        response.status_code = 500
        return response

    try:
        data = json.loads(parse_response(response))

        if response.status_code != 200:
            status = response.status_code
            response = Response(json.dumps(data), mimetype="application/json")
            response.status_code = status
            return response

        result = data['SOAP_ENV:Envelope']['SOAP_ENV:Body']['ConsultazioneIndicatoreResponse'][
            'ConsultazioneIndicatoreResult']
        if result['Esito'] == 'DATI_NON_TROVATI':
            response = Response(json.dumps(result['DescErrore']), mimetype="application/json")
            response.status_code = 404
        elif result['Esito'] == 'OK':
            response = Response(json.dumps(result['XmlEsitoIndicatore']['Indicatore']), mimetype="application/json")
            response.status_code = 200
        else:
            response = Response(json.dumps(result['DescErrore']), mimetype="application/json")
            response.status_code = 400
    except Exception as ex:
        response = Response("An error occurred while parsing ISEE", mimetype="application/json")
        response.status_code = 500
        return response

    return response
