from flask import Flask, Response, request
from utils import get_isee, get_wsdl
import os

app = Flask(__name__)

error = False
if not os.environ.get("WSDL_URL"):
    print("Missing WSLD_URL. Exiting...")
    error = True
if not os.environ.get("AUTH_HEADER"):
    print("Missing AUTH_HEADER. Exiting...")
    error = True
if not os.environ.get("CERTIFICATE"):
    print("Missing CERTIFICATE. Exiting...")
    error = True
if not os.environ.get("USERNAME"):
    print("Missing USERNAME. Exiting...")
    error = True
if not os.environ.get("PASSWORD"):
    print("Missing PASSWORD. Exiting...")
    error = True

if error:
    exit(1)

@app.route('/isee/<fiscal_code>')
def isee(fiscal_code):
    TEST = os.environ.get("TEST", "false").lower() in (True, 'true')
    if TEST:
        fiscal_code = os.environ.get("TEST_CF", fiscal_code)
    header = request.headers.get(os.environ.get("AUTH_HEADER")) or None
    if not TEST and fiscal_code != header:
        response = Response("Unauthorized", mimetype="application/json")
        response.status_code = 403
        return response
    return get_isee(fiscal_code=fiscal_code)


@app.route('/wsdl')
def wsdl():
    if os.environ.get("DEBUG"):
        return get_wsdl()
    else:
        response = Response("Unauthorized", mimetype="application/json")
        response.status_code = 403
        return response


if __name__ == '__main__':
    app.run(debug=True)
