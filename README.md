# ISEE REST API

API to obtain information relating to a citizen's ISEE given his fiscal code

## Installation

### Activate virtualenv

     . venv/bin/activate
     
### Install requirements

    . pip install requirements.txt
    
### Execution

    flask run

### Configuration

It is necessary to define some environment variables before running the application:
 
| Name                  |   Description                                                                         | Required  |
|-----------------------|---------------------------------------------------------------------------------------|-----------|
|   DEBUG               |   Activate debug mode?                                                                |   Yes     |   
|   FLASK_ENV           |   Flask environment                                                                   |   Yes     | 
|   FLASK_APP           |   Flask app file                                                                      |   Yes     |
|   WSDL_URL            |   Url called for soap requests                                                        |   Yes     |
|   AUTH_HEADER         |   Header's name used to authenticate user                                             |   Yes     |
|   CERTIFICATE         |   Certificate used for soap requests                                                  |   Yes     |
|   USERNAME            |   Username used to authenticate soap requests                                         |   Yes     |
|   PASSWORD            |   Password used to authenticate soap requests                                         |   Yes     |
|   CODICE_ENTE         |   Entity requesting the ISEE                                                          |   Yes     |
|   CODICE_UFFICIO      |   Office requesting the ISEE                                                          |   No      |
|   CFOPERATORE         |   Operator's fiscal code requesting the ISEE                                          |   No      |
|   TEST                |   Simulate user?                                                                      |   No      |
|   TEST_CF             |   Simulated user fiscal code; if not provided will be used the url parameter value    |   No      |

#### Test mode

For test purposes it is possible to launch the application in TEST mode by setting the value of the environment variable `TEST` to `true`.
If the `TEST_CF` parameter is defined, the requests use the fiscal code defined, otherwise the parameter defined in the url is used

## ISEE WSDL

Endpoint to consult the wdsl definition: accessible only if the application is launched in debug mode

Request `GET /wsdl`
 
## ISEE

Method for the recovery of the citizen's ISEE with fiscal code `fiscal_code`

Request `GET /isee/<fiscal_code>`

Response `200`:
    
    {
      "TipoIndicatore": "Ordinario",
      "ISE": "11111.11",
      "ScalaEquivalenza": "1.11",
      "ISEE": "11111.11",
      "ISR": "11111.11",
      "ISP": "11111.11",
      "DataPresentazione": "2020-01-01",
      "ProtocolloDSU": "INPS-ISEE-2020-12345678G-00",
      "RicercaCF": {
        "CodiceFiscale": "fiscal_code",
        "PrestazioneDaErogare": "A1.01",
        "ProtocolloDomandaEnteErogatore": "PROTOCOLLO",
        "StatodomandaPrestazione": "Da Erogare"
      }
    }
    
Response `404`:

    "Nessun indicatore trovato per: codice: Ordinario; Codice fiscale=fiscal_code; data validità="
    
The endpoint is protected by a check on the header defined in the environment variables (`AUTH_HEADER`) and the `fiscal_code` parameter.

It is possible to filter the results by year using the parameter in querystring `year` [`/isee/AAAAAA00A00?year=2020`]: 
by default (if not specified), only the ISEE of the **current year** will be considered